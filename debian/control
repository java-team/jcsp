Source: jcsp
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Section: java
Priority: optional
Build-Depends: ant,
               ant-contrib,
               debhelper-compat (= 13),
               default-jdk,
               javahelper,
               maven-repo-helper
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/java-team/jcsp.git
Vcs-Browser: https://salsa.debian.org/java-team/jcsp
Rules-Requires-Root: no

Package: libjcsp-java
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: libjcsp-java-doc (= ${binary:Version})
Description: Java library providing Communicating Sequential Processes (CSP) features
 JCSP (Communication Sequential Processes for Java) is a
 library providing a concurrency model that is a combination
 of ideas from Hoare's CSP and Milner's pi-calculus.
 .
 Communicating Sequential Processes (CSP) is a mathematical
 theory for specifying and verifying complex patterns of
 behaviour arising from interactions between concurrent
 objects.
 .
 JSCP provides a base range of CSP primitives plus a rich set of
 extensions. Also included is a package providing CSP process
 wrappers giving a channel interface to all Java AWT widgets
 and graphics operations.  It is extensively (javadoc)umented
 and includes much teaching.
 .
 JCSP is an alternative concurrency model to the threads and
 mechanisms built into Java. It is also compatible with
 it since it is implemented on top of it.

Package: libjcsp-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Suggests: libjcsp-java (= ${binary:Version})
Description: Documentation for libjcsp-java
 Documentation for JCSP (Communication Sequential Processes for
 Java) that is a library providing a concurrency model that is a
 combination of ideas from Hoare's CSP and Milner's pi-calculus.
 .
 Communicating Sequential Processes (CSP) is a mathematical
 theory for specifying and verifying complex patterns of
 behaviour arising from interactions between concurrent
 objects.
 .
 JSCP provides a base range of CSP primitives plus a rich set of
 extensions. Also included is a package providing CSP process
 wrappers giving a channel interface to all Java AWT widgets
 and graphics operations.  It is extensively (javadoc)umented
 and includes much teaching.
 .
 JCSP is an alternative concurrency model to the threads and
 mechanisms built into Java. It is also compatible with
 it since it is implemented on top of it.
